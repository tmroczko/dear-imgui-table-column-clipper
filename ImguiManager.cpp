#include "ImguiManager.h"
#include "imgui.h"
#include <string>
#include "imgui_internal.h"
#include "ImColumnClipper.h"
#include <vector>

void ImManager::DrawTable() {
	ImColumnClipper columnClipper = ImColumnClipper();

	idxOfLastRenderedColumn = 0;
	idxOfFirstRenderedColumn = columns;

	ImGui::SetNextWindowSizeConstraints(ImVec2(100, 100), ImVec2(1e10, 1e10));
	ImGui::Begin("Table", nullptr, ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_MenuBar);

	ImGui::BeginMenuBar();
	if (ImGui::BeginMenu("Table")) {
		if (ImGui::InputInt("Rows", &rows) && rows < 1) rows = 1;
		if (ImGui::InputInt("Columns", &columns) && columns < 1) columns = 1;
		ImGui::Checkbox("Frozen headers", &headers);
		if (ImGui::InputInt("Max data length", &maxL))// columnWidth=0;
		if (maxL <= 0) maxL = 1;
#ifndef NDEBUG
		if (columns > 511) columns = 511;
#endif
		if (ImGui::InputFloat("Column Width", &columnWidth)) columnClipper.ResetWidth();
		ImGui::EndMenu();
	}
	ImGui::EndMenuBar();

	drawnCells = 0;
	if (ImGui::BeginTable("TestTable", columns, ImGuiTableFlags_ScrollY | ImGuiTableFlags_ScrollX | ImGuiTableFlags_SizingFixedSame | ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg)) {
		for (int i = 0; i < columns; i++) {
			ImGui::TableSetupColumn(("###", std::to_string(i)).c_str(), ImGuiTableColumnFlags_WidthFixed, columnWidth);
			// scailing to headers works because it happens before drawing the table
		}
		if (headers) ImGui::TableSetupScrollFreeze(1, 1);

		columnClipper.Begin(columns, columnWidth, headers);

		ImGuiListClipper rowClipper = ImGuiListClipper();
		rowClipper.Begin(rows);
	
		ImGui::TableHeadersRow();
	
		while (rowClipper.Step()) {
			for (int i = rowClipper.DisplayStart; i < rowClipper.DisplayEnd; i++) {
				ImGui::TableNextRow();
				while (columnClipper.Step()) {
					for (int j = columnClipper.DisplayStart; j < columnClipper.DisplayEnd; j++) {
						ImGui::TableSetColumnIndex(j);
						drawnCells++;
						std::string val;
						// srand here with seed dependant on cell position to generate random but consistent accross frames table content
						srand(i*3+i*5*j*7+j*11+13);
						int L = (rand() % maxL);
						for (int k = 0; k <= L; k++) {
							val += (rand() % ('Z' - 'a')) + 'a';
						}
						ImGui::Text(val.c_str());
						if (j > idxOfLastRenderedColumn) idxOfLastRenderedColumn = j;
						if (j < idxOfFirstRenderedColumn) idxOfFirstRenderedColumn = j;
					}
				}
			}
		}
		ImGui::EndTable();
	}
	ImGui::End();
}

void ImManager::DrawInfo() {
	ImGui::Begin("Info");
	ImGui::Text("Index of first drawn column:"); ImGui::SameLine();
	ImGui::Text(std::to_string(idxOfFirstRenderedColumn).c_str());
	ImGui::Text("Index of last drawn column:"); ImGui::SameLine();
	ImGui::Text(std::to_string(idxOfLastRenderedColumn).c_str());
	ImGui::Text("Number of drawn cells:"); ImGui::SameLine();
	ImGui::Text(std::to_string(drawnCells).c_str());
	ImGui::End();
}

void ImManager::Render()
{
	DrawTable();
	DrawInfo();
}

