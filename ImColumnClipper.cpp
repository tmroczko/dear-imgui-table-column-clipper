#include "ImColumnClipper.h"
#include <string>

void ImColumnClipper::Begin(int _columns, float _columnWidth, bool _frozenHeaderColumn)
{
	ImGuiContext& g = *ImGui::GetCurrentContext();

	// validity checks
	window = g.CurrentWindow;
	if (!window) throw std::exception("Cannot use ImColumnClipper outside a window");

	table = g.CurrentTable;
	if (!table) throw std::exception("Cannot use ImColumnClipper outside a table");

	if (!(table->Flags & ImGuiTableFlags_SizingFixedSame)) throw std::exception("Cannot use ImColumnClipper with variable column size");

	// get the needed values
	StartPosX = window->DC.CursorPos.x;
	lossyness = window->DC.CursorStartPosLossyness.x; // measures precision loss in drawing
	columns = _columns;
	if (_columnWidth + addedWidth - lossyness > columnWidth) {
		columnWidth = _columnWidth + addedWidth - lossyness;
		if (firstWidth) firstWidth = false;
		else resetScroll = true;
	}
	minVisibleX = window->ClipRect.Min.x;
	visibleWidth = window->ClipRect.Max.x - minVisibleX;
	frozenHeaderColumn = _frozenHeaderColumn;
}

bool ImColumnClipper::Step()
{
	if (resetScroll) {
		ImGui::SetScrollX(0);
		resetScroll = false;
	}
	if (frozenHeaderColumn && step == 2) { // already drew frozen(step 0) and regular (step 1) columns
		step = 0;
		minVisibleX -= columnWidth;
		return false;
	}
	if (frozenHeaderColumn && step==0) { // has frozen column, draw the one frozen column (step 0)
		DisplayStart = 0;
		DisplayEnd = 1;
		minVisibleX += columnWidth;
		step++;
		return true;
	}
	if (!frozenHeaderColumn && step != 0) { // no frozen column, already drawn regular columns
		step = 0;
		return false;
	}
	// draw regular columns
	if (abs((minVisibleX - StartPosX)) < 1e-5) DisplayStart = 0; // needed to avoid error when the first column is fully visible
	else DisplayStart = ((minVisibleX - StartPosX - lossyness) / columnWidth) - 0.5f + frozenHeaderColumn*0.5f;
	DisplayEnd = DisplayStart + ((visibleWidth + columnWidth + lossyness) / columnWidth) - (frozenHeaderColumn) + 0.999999f;

	// validate so only existing columns are drawn
	int diff = 0;
	if (DisplayEnd > columns) {
		diff = DisplayEnd - columns;
		DisplayEnd = columns;
		DisplayStart -= diff;
	}
	if (DisplayStart < 0) DisplayStart = 0+frozenHeaderColumn;
	if (DisplayEnd < 1) DisplayEnd = 1;
	step++;
	return true;
}

void ImColumnClipper::ResetWidth()
{
	columnWidth = 0;
	resetScroll = true;
	firstWidth = false;
}
