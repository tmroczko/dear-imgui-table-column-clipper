#pragma once

#include "imgui.h"
#include "imgui_internal.h"

//Columns must be constant-width, no resizing, the width must be passed to Begin(), maximum of frozen columns is 1, pass true as the third argument if it is
class ImColumnClipper {
public:
	//Call Begin inside ImGui Table, after TableSetupColumn and TableSetupScrollFreeze, but before TableHeadersRow and the table draw loop
	void Begin(int _columns, float _columnWidth, bool _frozenHeaderColumn = false);

	//Enclose the innermost (cell drawing) loop inside while(Step()) loop, this must be inside the row drawing loop
	bool Step();

	//Pass as start value value in the cell drawing for loop
	int DisplayStart = 0;

	//Pass as < condition value in the cell drawing for loop
	int	DisplayEnd = 1;

	void ResetWidth();
private:
	float addedWidth = 9; // constant width that is always added to the set width (could perhaps depend on styling, borders etc.)
	unsigned short step = 0;
	bool frozenHeaderColumn = false;
	int columns, columnWidth;
	ImGuiWindow* window;
	ImGuiTable* table;
	float lossyness;
	float StartPosX, minVisibleX, visibleWidth;
	bool resetScroll = false;
	bool firstWidth = true;
};