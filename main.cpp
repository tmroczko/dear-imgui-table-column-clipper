#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "imgui.h"

#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include <string>
#include <random>

#include "ImguiManager.h"

#include "thirdparty/imgui_test_engine/imgui_te_engine.h"
#include "thirdparty/imgui_test_engine/imgui_te_ui.h"
#include "thirdparty/imgui_test_engine/imgui_te_context.h"

void RegisterTests(ImGuiTestEngine* e, ImManager* m_);

class TestInterface {
public:
    TestInterface(ImManager* m_) { m = m_; };
    ImManager* m;
    int GetColumns() { return m->columns; };
    int GetDrawnColumns() { return m->idxOfLastRenderedColumn - m->idxOfFirstRenderedColumn + 1; };
    bool IsHeader() { return m->headers; };
    float GetWidth() { return m->columnWidth; };
};

int main()
{
    // Initialize GLFW
    if (!glfwInit())
        return -1;

    // Create a windowed mode window and its OpenGL context
    GLFWwindow* window = glfwCreateWindow(640, 480, "ImGui Hello World", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

    // Make the window's context current
    glfwMakeContextCurrent(window);
    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

    // Setup ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGuiTestEngine* engine = ImGuiTestEngine_CreateContext();
    ImGuiTestEngineIO& test_io = ImGuiTestEngine_GetIO(engine);
    test_io.ConfigVerboseLevel = ImGuiTestVerboseLevel_Info;
    test_io.ConfigVerboseLevelOnError = ImGuiTestVerboseLevel_Debug;
    ImGuiTestEngine_Start(engine, ImGui::GetCurrentContext());
    ImGuiTestEngine_InstallDefaultCrashHandler();


    ImGui::StyleColorsDark();

    // Setup ImGui bindings for GLFW and OpenGL
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 130");

    ImManager man = ImManager();
    RegisterTests(engine, &man);

    srand(0);

    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        // Poll events
        glfwPollEvents();

        // ImGui new frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        // ImGui content
        man.Render();
        static bool showTestUi = true;
        showTestUi = true;
        ImGuiTestEngine_ShowTestEngineWindows(engine, &showTestUi);

        // Render ImGui
        ImGui::Render();

        // OpenGL rendering
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClear(GL_COLOR_BUFFER_BIT);
        
        // ImGui OpenGL render
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        // Swap buffers
        glfwSwapBuffers(window);
        ImGuiTestEngine_PostSwap(engine);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGuiTestEngine_Stop(engine);
    ImGui::DestroyContext();
    ImGuiTestEngine_DestroyContext(engine);

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}

void RegisterTests(ImGuiTestEngine* e, ImManager* m) {
    ImGuiTest* t = NULL;
#ifndef NDEBUG
    t = IM_REGISTER_TEST(e, "TableWindow", "Column Limiter");
    t->TestFunc = [m](ImGuiTestContext* ctx) {
        TestInterface data(m);
        ctx->SetRef("Table");
        ctx->MenuClick("Table");
        // too large
        ctx->MenuClick("Table/Columns");
        ctx->KeyCharsReplaceEnter("600");
        IM_CHECK_EQ(data.GetColumns(), 511);
        // NaN
        ctx->MenuClick("Table/Columns");
        ctx->KeyCharsReplaceEnter("abc");
        IM_CHECK_EQ(data.GetColumns(), 511);
        // too low
        ctx->MenuClick("Table/Columns");
        ctx->KeyCharsReplaceEnter("-10");
        IM_CHECK_EQ(data.GetColumns(), 1);
        };
#endif
    t = IM_REGISTER_TEST(e, "TableWindow", "Column Clipping Resize");
    t->TestFunc = [m](ImGuiTestContext* ctx) {
        TestInterface data(m);
        ImGui::SetWindowSize(ctx->GetWindowByRef("Table"), ImVec2(300, 200));
        ctx->SetRef("Table");
        ImGuiWindow* window = ctx->GetWindowByRef("Table");
        if (data.GetColumns() != 100) {
            ctx->MenuClick("Table");
            ctx->MenuClick("Table/Columns");
            ctx->KeyCharsReplaceEnter("100");
            ctx->MouseMoveToPos(window->Pos*1.5);
            ctx->MouseClick();
        }
        if (data.GetWidth() != 30) {
            ctx->MenuClick("Table");
            ctx->MenuClick("Table/Column Width");
            ctx->KeyCharsReplaceEnter("30");
            ctx->MouseMoveToPos(window->Pos * 1.5);
            ctx->MouseClick();
        }
        ctx->MouseSetViewport(window);
        ctx->MouseMoveToPos(window->Pos + ((window->Size - ImVec2(1.0f, 1.0f)) * ImVec2(1,0.5)));
        ctx->MouseDragWithDelta(ImVec2(100,0));
        IM_CHECK_EQ(data.GetDrawnColumns(), 11);
        };
    t = IM_REGISTER_TEST(e, "TableWindow", "Column Clipping Scroll");
    t->TestFunc = [m](ImGuiTestContext* ctx) {
        TestInterface data(m);
        ImGui::SetWindowSize(ctx->GetWindowByRef("Table"), ImVec2(300, 200));
        ctx->SetRef("Table");
        ImGuiWindow* window = ctx->GetWindowByRef("Table");
        if (data.GetColumns() != 100) {
            ctx->MenuClick("Table");
            ctx->MenuClick("Table/Columns");
            ctx->KeyCharsReplaceEnter("100");
            ctx->MouseMoveToPos(window->Pos * 1.5);
            ctx->MouseClick();
        }
        if (data.GetWidth() != 30) {
            ctx->MenuClick("Table");
            ctx->MenuClick("Table/Column Width");
            ctx->KeyCharsReplaceEnter("30");
            ctx->MouseMoveToPos(window->Pos * 1.5);
            ctx->MouseClick();
        }
        ctx->MouseMoveToPos(window->Pos + ((window->Size - ImVec2(1.0f, 1.0f)) * ImVec2(0.1, 0.925)));
        ctx->MouseDragWithDelta(ImVec2(100, 0));
        IM_CHECK_EQ(data.GetDrawnColumns(), 8);
        };
    t = IM_REGISTER_TEST(e, "Self Test", "Item in child window");
    t->GuiFunc = [](ImGuiTestContext* ctx) {
        ImGui::Begin("Parent", nullptr, ImGuiWindowFlags_AlwaysAutoResize);
        ImGui::Button("ButtonP");
        ImGui::BeginChild("Child",ImVec2(100,100));
        ImGui::Button("ButtonC");
        ImGui::EndChild();
        ImGui::End();
        };
    t->TestFunc = [](ImGuiTestContext* ctx) {
        ctx->SetRef("Parent");
        ctx->ItemClick("ButtonP");
        ctx->ItemClick("//Parent/**/ButtonC");
        };
}
