#pragma once

class ImManager {
public:
	void Render();
	void DrawTable();
	void DrawInfo();
private:
	int columns = 10, rows = 10, drawnCells = 0, maxL = 10;
	bool headers = false;
	float columnWidth = 30;

	// for debug only
	int idxOfLastRenderedColumn = 0;
	int idxOfFirstRenderedColumn = 0;
	friend class TestInterface;
};